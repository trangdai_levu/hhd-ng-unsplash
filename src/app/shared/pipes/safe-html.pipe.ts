/**
 * https://gist.github.com/klihelp/4dcac910124409fa7bd20f230818c8d1
 */

import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Pipe({
  name: 'safeHTML'
})
export class SafeHTMLPipe implements PipeTransform {

  constructor(private domSanitizer: DomSanitizer) {
  }

  public transform(value: string, args: any): SafeHtml {
    return this.domSanitizer.bypassSecurityTrustHtml(value);
  }
}
