import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';

@Pipe({
  name: 'safeStyle'
})
export class SafeStylePipe implements PipeTransform {

  constructor(private domSanitizer: DomSanitizer) {
  }

  public transform(value: string, args?: any): SafeStyle {
    return this.domSanitizer.bypassSecurityTrustStyle(value);
  }
}
