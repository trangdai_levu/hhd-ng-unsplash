import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  SafeHTMLPipe
} from './pipes';

@NgModule({
  declarations: [
    SafeHTMLPipe
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
