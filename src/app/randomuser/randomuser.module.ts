import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayersRoutingModule } from './randomuser-routing.module';
import { NamecardComponent } from './namecard/namecard.component';
import { UserPhotosComponent } from './user-photos/user-photos.component';

@NgModule({
  imports: [
    CommonModule,
    PlayersRoutingModule
  ],
  declarations: [
    NamecardComponent,
    UserPhotosComponent
  ],
})
export class RandomuserModule { }
