import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NamecardComponent } from './namecard/namecard.component';
import { UserPhotosComponent } from './user-photos/user-photos.component';

const routes: Routes = [
  { path: '', redirectTo: '/randomuser/namecard' , pathMatch: 'full' },
  {
    path: 'namecard',
    component: NamecardComponent,
  },
  {
    path: 'user-photos',
    component: UserPhotosComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlayersRoutingModule { }
