import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NgOriginalComponent } from './ng-original';
import { RandomuserModule } from './randomuser';

const routes: Routes = [
  {
    path: '',
    component: NgOriginalComponent
  },
  {
    path: 'randomuser',
    loadChildren: () => RandomuserModule,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
