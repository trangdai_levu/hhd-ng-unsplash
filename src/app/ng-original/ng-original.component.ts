import { Component } from '@angular/core';

@Component({
  selector: 'app-ng-original',
  templateUrl: './ng-original.component.html',
})
export class NgOriginalComponent {
  title = 'hhd-ng-unsplash';
}
