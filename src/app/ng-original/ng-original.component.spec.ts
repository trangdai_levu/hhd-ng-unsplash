import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgOriginalComponent } from './ng-original.component';

describe('Component::: NgOriginal', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        NgOriginalComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(NgOriginalComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'hhd-ng-unsplash'`, () => {
    const fixture = TestBed.createComponent(NgOriginalComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('hhd-ng-unsplash');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(NgOriginalComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('hhd-ng-unsplash app is running!');
  });
});
